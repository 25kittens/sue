module codeberg.com/numiko/sue

go 1.17

require (
	github.com/pelletier/go-toml/v2 v2.0.0-beta.4
	github.com/shkh/lastfm-go v0.0.0-20191215035245-89a801c244e0
	maunium.net/go/mautrix v0.10.4
)

require (
	github.com/btcsuite/btcutil v1.0.2 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/net v0.0.0-20211020060615-d418f374d309 // indirect
)
