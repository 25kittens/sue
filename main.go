// suebot project main.go
package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/pelletier/go-toml/v2"
	"github.com/shkh/lastfm-go/lastfm"
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/event"
	"maunium.net/go/mautrix/format"
)

type Lastfm struct {
	Key    string
	Secret string
}

type Matrix struct {
	Username   string
	Homeserver string
	Password   string
}

type Config struct {
	Prefix string
	Matrix Matrix
	Lastfm Lastfm
}

// important global vars
var cfg Config
var sue *mautrix.Client
var lfm *lastfm.Api

// profoundly stupid error check function
func check(e error) {
	if e != nil {
		panic(e)
	}
}

func setupConfig(path string, conf *Config) (err error) {
	// config data file is read
	dat, err := os.ReadFile(path)
	if err != nil {
		return
	}

	// read toml
	err = toml.Unmarshal([]byte(string(dat)), &conf)
	return
}

func setupMatrix(homeserver, user, password string) (client *mautrix.Client, err error) {
	// create client
	client, err = mautrix.NewClient(homeserver, "", "")
	if err != nil {
		return
	}

	// login
	_, err = client.Login(&mautrix.ReqLogin{
		Type:             "m.login.password",
		Identifier:       mautrix.UserIdentifier{Type: mautrix.IdentifierTypeUser, User: user},
		Password:         password,
		StoreCredentials: true,
	})

	if err == nil {
		fmt.Println("logged in")
	}
	return
}

func main() {
	var err error

	// sets up config var
	err = setupConfig("config.toml", &cfg)
	check(err)

	/*	i tried actually passing sue as &sue in the parameters
		for this function but since the setup process is a
		goroutine all it did is call for matrix events before
		it even logged in. by returning the actual client
		like this it blocks the program until it returns a
		fully logged in client. */
	sue, err = setupMatrix(cfg.Matrix.Homeserver, cfg.Matrix.Username, cfg.Matrix.Password)
	check(err)

	// init last.fm api
	lfm = lastfm.New(cfg.Lastfm.Key, cfg.Lastfm.Secret)

	// init syncer process
	oei := &mautrix.OldEventIgnorer{UserID: sue.UserID}
	syncer := sue.Syncer.(mautrix.ExtensibleSyncer)
	oei.Register(syncer)

	// handles incoming message events
	syncer.OnEventType(event.EventMessage, func(_ mautrix.EventSource, evt *event.Event) {
		// don't reply to yourself :/
		if evt.Sender == sue.UserID {
			return
		}

		// passes event to the command handler asynchronously
		go handleCommand(evt)
	})

	// not sure but i think this is the main loop?
	// it was pure luck that this worked lol
	err = sue.Sync()
	check(err)

}

// profoundly stupid command handler
func handleCommand(evt *event.Event) {
	// i put the content string outside of
	// the if block so that the bot doesn't
	// call strings.ToLower n stuff for
	// every single message
	var content = evt.Content.AsMessage().Body
	var split []string

	if strings.HasPrefix(content, cfg.Prefix) {
		content = strings.ToLower(content)
		split = strings.SplitN(content, " ", 2) // splits the command into args
		var msg string

		// commands
		switch strings.TrimPrefix(split[0], cfg.Prefix) {
		case "ping":
			pongCommand(&msg)
		case "fm":
			fmCommand(split, &msg) // here i pass the 'split' args into .fm!!
		}

		if msg != "" {
			// send the generated 'msg' string by the command (as markdown!!)
			sue.SendMessageEvent(evt.RoomID, event.EventMessage, format.RenderMarkdown(msg, true, false))
		} else {
			// for debug purposes, if 'msg' ends up being empty
			// then it prints out a lil notice in the console
			fmt.Println("'msg' var is empty!!")
		}
	}
}

/*	COMMANDS
they should all modify the 'msg'
variable in the above commandHandle()
function rather than return it. also,
msg should be formatted in markdown
(as seen above). */

// simplest of commands
func pongCommand(r *string) {
	*r = "pong!!!!!!"
}

// .fm command
// TODO: store users to avoid typing username every time
func fmCommand(args []string, r *string) {
	// easier to manage variables by
	// declaring them this way rather
	// than doing ":=" (ie if you need
	// to swap their places around)
	var resp lastfm.UserGetRecentTracks
	var err error
	var status string

	// checks if args were given
	if len(args) < 2 {
		fmt.Println(".fm: ", args)
		*r = "no user provided!!"
	} else {
		fmt.Println(".fm: ", args) // for debug purposes

		// stores the given user's recent tracks into 'resp'
		resp, err = lfm.User.GetRecentTracks(lastfm.P{"user": args[1]})
		if err != nil {
			// absolutely incredible error handlin,g
			fmt.Println(err)
			*r = fmt.Sprintf("**error!!**: `%s`", err.Error())
		} else if len(resp.Tracks) < 1 {
			// in case user has no scrobbles
			// still can't believe it crashed the bot at first
			fmt.Printf("user '%s' has no tracks", args[1])
			*r = fmt.Sprintf("**error**: user **'%s'** has no scrobbled tracks!!", args[1])
		} else {
			// manages the 'NowPlaying' part of the api
			if s, _ := strconv.ParseBool(resp.Tracks[0].NowPlaying); s {
				status = "now playing"
			} else {
				status = "last played"
			}

			// final response ;)
			*r = fmt.Sprintf("%s: **%s** by %s | from **%s**", status, resp.Tracks[0].Name, resp.Tracks[0].Artist.Name, resp.Tracks[0].Album.Name)
		}
	}
}
